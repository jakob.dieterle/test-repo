import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Agg')


# merge left and right into list in ascending order
def merge(left, right, list):

    # init indices for left list, right list, and the complete list
    l = 0
    r = 0
    i = 0

    # assign elements of left and right to the list and keeping ascending order 
    while l < len(left) and r < len(right):
        if left[l] <= right[r]:
            list[i] = left[l]
            l += 1
        else:
            list[i] = right[r]
            r += 1
        i += 1

    # if left list is longer: add the remaining elements to list
    while l < len(left):
        list[i] = left[l]
        l += 1
        i += 1

    # if right list is longer: add the remaining elements to list
    while r < len(right):
        list[i] = right[r]
        r += 1
        i += 1


# sort the given list in place to ascending order using the mergesort algorithm
def merge_sort(list):

    # arrays with less than 2 elements do not need to be sorted
    if (len(list) <= 1):
        return
    
    # determine cutoff at the middle of the list
    mid = len(list) // 2
    # save sub lists 
    left = list[:mid]
    right = list[mid:]

    # apply mergesort to both lists
    merge_sort(left)
    merge_sort(right)

    # merge sub lists 
    merge(left, right, list)


# list to be sorted
my_list = [54, 26, 93, 17, 77, 31, 44, 55, 20]

# plot unsorted list
x = range(len(my_list))
plt.plot(x, my_list)

# sort the list
merge_sort(my_list)

# plot sorted list
x = range(len(my_list))
plt.plot(x, my_list)
plt.savefig("plot.png")
